# Incubator

## Install

```sh
sudo apt install nodejs yarnpkg node-gyp
yarnpkg install # potentionally with --force
```

## System

- Arduino Uno with Firmata firmware
- H-Bridge motor driver driving thermoelectric module (Heating/Cooling)
- Si7021 Humidity/Temperature Sensor
- Internal/External Fan PWM driven (mounted on TEM)

## Run

```sh
yarnpkg start
```

## Upgrade dependencies

```sh
yarnpkg upgrade --latest
```

## Operate

- exit app -> ```.exit```
- set temperature setpoint -> ```set(temp_in_celsius)```
- set exothermal temp setpoint -> ```set2(temp_in_celsius)```
- set time left -> ```time(hours)```
- start manual (use setpoint above) -> ```start()```
- start tempeh recipe -> ```tempeh()```
- start yoghurt recipe -> ```yoghurt()```
- reset controller -> ```reset```
