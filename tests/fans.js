var five = require("johnny-five");
var board = new five.Board({port: "/dev/ttyUSB0"});


board.on("ready", function() {

  fan_outside = new five.Motor({
    pin: 11
  });

  fan_inside = new five.Motor({
    pin: 3
  }); 
  fan_inside_pin = new five.Pin(2);

  // Reverse the motor at maximum speed
  fan_outside.start(150)
//   m1.stop(50);

  fan_inside_pin.high();
  fan_inside.start();

  this.on("exit", function() {
    fan_inside.stop();
    fan_inside_pin.low();
    fan_outside.stop();
  });

});
