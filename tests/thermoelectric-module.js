var five = require("johnny-five");
var board = new five.Board({port: "/dev/ttyUSB0"});

SHIELD_CONFIGS = {
    ELECROW_ACS70028DH: {
    // https://www.elecrow.com/wiki/index.php?title=Dual_Channel_H-Bridge_Motor_Shield
        Motor_1: {
            pins: {
                pwm: 9,
                dir: 4,
                cdir: 5,
                enable: 6
            }
        },
        Motor_2: {
            pins: {
                pwm: 10,
                dir: 7,
                cdir: 8,
                enable: 6
            }
        }  
    }
};


board.on("ready", function() {

  var m1 = new five.Motor(SHIELD_CONFIGS.ELECROW_ACS70028DH.Motor_1);

  // Reverse the motor at maximum speed
  m1.enable()
  m1.reverse(50);
  m1.disable()

});
