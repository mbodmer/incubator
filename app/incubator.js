var five = require('johnny-five');
let Controller = require('node-pid-controller');
var { DateTime, Interval, Duration } = require('luxon');
var CircularBuffer = require("circular-buffer");
var parquet = require('parquetjs-lite');

// Configuration
SHIELD_CONFIGS = {
    ELECROW_ACS70028DH: {
        // https://www.elecrow.com/wiki/index.php?title=Dual_Channel_H-Bridge_Motor_Shield
        Motor_1: {
            pins: {
                pwm: 9,
                dir: 4,
                cdir: 5,
                enable: 6,
                frequency: 1000
            }
        },
        Motor_2: {
            pins: {
                pwm: 10,
                dir: 7,
                cdir: 8,
                enable: 6,
                frequency: 1000
            }
        }
    },
    PWM_FAN_1: {
        pin: 3
    },
    PWM_FAN_2: {
        pin: 11
    },
    MULTI_SENSOR_1: {
        controller: "SI7021"
    }
};

function log_timestamped_message(msg) {
    console.log(DateTime.local().toISO() + "\t" + msg);
}


class Incubator {
    constructor() {
        this._rh = 0.0;
        this._temp = 0.0;
        this._temp_set = 20.0;
        this._temp_set2 = 20.0;
        this._power = 0.0

        // Setup Hardware
        this._heating = new five.Motor(SHIELD_CONFIGS.ELECROW_ACS70028DH.Motor_1);
        this._fan_in = new five.Motor(SHIELD_CONFIGS.PWM_FAN_1);
        this._fan_in_pin = new five.Pin(2);
        this._fan_out = new five.Motor(SHIELD_CONFIGS.PWM_FAN_2);
        this._climate = new five.Multi(SHIELD_CONFIGS.MULTI_SENSOR_1);

        this.reset();
    }

    enable() {
        this._fan_in_pin.high();
        this._fan_in.start();
        this._fan_out.start();
        this._heating.enable();
        this._heating.stop();
        log_timestamped_message("incubator enabled ");
    }

    disable() {
        this._heating.stop();
        this._fan_out.stop();
        this._fan_in.stop();
        this._heating.disable();
        log_timestamped_message("incubator disabled ");
    }

    set power(power) {
        var pwr = Math.floor(power);
        if (pwr > 0) {
            if (pwr > 250) pwr = 250; // limit to 250
            // heating, big heatsink is outside, we can stop the fan
            this._fan_out.stop();
            this._fan_in.start();
            this._heating.forward(Math.abs(pwr));
        } else if (pwr < 0) {
            if (pwr < -250) pwr = -250;
            // cooling, get the heat out, enable fan on outside
            this._fan_out.start(255);
            this._fan_in.start();
            this._heating.reverse(Math.abs(pwr));
        } else {
            this._heating.stop();
        }
        this._power = pwr;
        // update moving average power to detect exothermal process
        this._power_avg.enq(pwr);
    }

    get power() {
        return this._power;
    }

    // current temperature measured
    get temp() {
        return this._temp;
    }

    set temp(temp) {
        this._temp = temp;
    }

    // normal temperature setpoint
    get temp_set() {
        return this._temp_set;
    }

    set temp_set(temp) {
        this._temp_set = temp;
    }

    // exothermal temperature setpoint
    get temp_set2() {
        return this._temp_set2;
    }

    set temp_set2(temp) {
        this._temp_set2 = temp;
    }

    // measured humidity
    get rh() {
        return this._rh;
    }

    set rh(rh) {
        this._rh = rh;
    }

    // start timing
    start(duration) {
        const now = DateTime.local();
        this._time_interval = Interval.after(now, duration);
        this._isStarted = true;
    }

    get isStarted() {
        return this._isStarted;
    }

    // return time left
    get time_left() {
        const now = DateTime.local();
        if (this._time_interval.contains(now)) {
            return Interval.fromDateTimes(now, this._time_interval.end).toDuration();
        }
        return Duration.fromMillis(0);
    }

    set time_left(hours) {
        const time_left_now = this.time_left;
        const new_time_from_now = Duration.fromObject({ hours: hours });
        const time_extension = new_time_from_now.minus(time_left_now);
        const old_duration = this._time_interval.toDuration();
        const new_duration = old_duration.plus(time_extension);
        this._time_interval = Interval.after(this._time_interval.start, new_duration);
        this._isStarted = true;
    }

    reset() {
        const zero = DateTime.fromMillis(0);
        this._time_interval = Interval.fromDateTimes(zero, zero);
        this._isStarted = false;
        this._power = 0.0;
        this._power_avg = new CircularBuffer(180); // 15mins at 5 sec interval
    }

    get power_avg() {
        const values = this._power_avg.toarray();
        const sum = values.reduce((a, b) => a + b, 0);
        if (values.length == 0) {
            return 0;
        }
        return sum / values.length;
    }
};

async function contol_loop() {


    // declare a schema for the data logging file
    var datafile_schema = new parquet.ParquetSchema({
        timestamp: { type: 'TIMESTAMP_MILLIS' },
        temp_set: { type: 'DOUBLE' },
        temp: { type: 'DOUBLE' },
        rh: { type: 'DOUBLE' },
        pwr: { type: 'DOUBLE' },
        pwr_avg: { type: 'DOUBLE' }
    });

    // create data logging file
    var fs = require('fs');
    var dir = './logs';
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }
    var filename = "./logs/tempeh_" + new Date().toISOString().replace(/\:/g, '').replace(/\-/g, '').slice(0, 15) + ".parquet";
    var datafile_writer = await parquet.ParquetWriter.openFile(datafile_schema, filename);
    log_timestamped_message("data logfile created: " + filename);

    var board = new five.Board();

    // Begin program when the board, serial and
    // firmata are connected and ready
    board.on("ready", function () {

        console.log("\n");
        log_timestamped_message("Application started");

        let interval_millis = 5000;
        let interval_sec = interval_millis / 1000;

        var incubator = new Incubator();
        let controller = new Controller({
            k_p: 74.0,
            k_i: 0.08,
            k_d: 0.02,
            interval_sec
        });

        this.on("exit", function () {
            incubator.disable();
        });

        this.repl.inject({
            incubator: incubator,
            controller: controller,
            set: function (temp) {
                if (temp > 45) {
                    temp = 45;
                }
                if (temp < 5) {
                    temp = 5;
                }
                incubator.temp_set = temp;
            },
            set2: function (temp) {
                if (temp > 45) {
                    temp = 45;
                }
                if (temp < 5) {
                    temp = 5;
                }
                incubator.temp_set2 = temp;
            },
            time: function (hours) {
                incubator.time_left = hours;
            },
            start: function () {
                log_timestamped_message("starting manual control, set setpoint with: set(temp), time(t)");
                incubator.start(Duration.fromObject({ hours: 50, minutes: 0 }));
            },
            tempeh: function () {
                log_timestamped_message("starting tempeh recipe ... 35°C/29°C, 48h");
                incubator.temp_set = 35;
                incubator.temp_set2 = 29;
                incubator.start(Duration.fromObject({ hours: 48, minutes: 0 }));
            },
            yoghurt: function () {
                log_timestamped_message("starting yoghurt recipe ... 40°C, 4h");
                incubator.temp_set = 40;
                incubator.temp_set2 = 40;
                incubator.start(Duration.fromObject({ hours: 0, minutes: 3 }));
            },
            reset: function () {
                log_timestamped_message("resetting incubator");
                incubator.reset();
                incubator.enable();
            },
        });

        // update incubator state (todo: make more self contained)
        incubator._climate.on("change", function () {
            incubator.temp = this.thermometer.celsius;
            incubator.rh = this.hygrometer.relativeHumidity;
        });

        var process_is_done = false;

        incubator.enable();

        this.loop(interval_millis, () => {

            const pwr_avg = incubator.power_avg;
            const is_exothermal = (pwr_avg < 25);

            if (incubator.isStarted) {

                // set the current setpoint from incubator (temp) normally,
                // use temp2 as setpoint when the process seems exothermal
                if (is_exothermal) {
                    controller.setTarget(incubator.temp_set2);
                } else {
                    controller.setTarget(incubator.temp_set);
                }

                // update PID loop with current temperatuer
                let correction = controller.update(incubator.temp);

                // apply
                incubator.power = correction;

                // is time up?
                if (incubator.time_left.valueOf() == 0) {
                    // lower temperature to fridge temp
                    controller.setTarget(8);
                    process_is_done = true
                }
            }

            // print current status
            log_timestamped_message(
                " | Tset: " + controller.target.toFixed(1) +
                " | T: " + incubator.temp.toFixed(1) +
                " | RH: " + incubator.rh.toFixed(0) +
                " | P: " + incubator.power.toFixed(0) +
                " | Pm: " + pwr_avg.toFixed(0) +
                " | exo: " + is_exothermal +
                " | t: " + incubator.time_left.toFormat("h:m:s") +
                " | s: " + incubator.isStarted +
                " | d: " + process_is_done
            );

            datafile_writer.appendRow({
                timestamp: new Date(),
                temp_set: controller.target,
                temp: incubator.temp,
                rh: incubator.rh,
                pwr: incubator.power,
                pwr_avg: pwr_avg,
            });
        });
    });
}

contol_loop();
